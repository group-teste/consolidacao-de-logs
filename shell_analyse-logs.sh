#!/bin/bash

### Coleta dos dados de acesso ###
quantAcessos1=$(for files in $(find /logs/nginx_access*.log.gz -ctime -7) ; do zcat $files|grep /solr/select ; done|wc -l)
quantAcessos2=$(for files in $(find /logs/nginx_access*.log.gz -ctime -7) ; do zcat $files|grep /solr/query ; done|wc -l)
body="<html>  <body>  <h3> Resumo dos acessos</h3> <font size="2" color="black">Quantidade de acessos /solr/select: "$quantAcessos1" <br />  Quantidade de acessos /solr/query: "$quantAcessos2" </font> <br />  <font size="2" color="black">Origem dos acessos: </font>  <font size="2" color="black">"$(for files in $(find /logs/nginx_access*.log.gz -ctime -7);do zcat $files|grep -E "/solr/select|/solr/query"|awk '{ cmd="nslookup "$1; system(cmd) }'|awk '{ print $4 }'|grep "."|uniq; done)"</font> </body> </html>"
### Fim coleta dos dados de acesso ###


### Envio do email ###
remetente="<acessos_solr@email.com>"
subject="[Solr]: Acessos dos últimos 07 dias"
destinatario="<destinatario@email.com>"
(
echo "HELO domain.com"
echo "MAIL FROM: $remetente"
echo "RCPT TO: $destinatario"
echo "data"
echo "From: $remetente"
echo "To: $destinatario"
echo "Subject: $subject"
echo "Content-Type: text/html"
echo
echo "$body"
echo "."
sleep 1s
) | telnet smtp.damain.com 25
### Fim envio do email ###
